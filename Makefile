.PHONY: clean

CC       = gcc
CFLAGS   = $(if $(DEBUG),-DDEBUG -g -O0,-O3) -Wall
BINARIES = qsort

qsort_SOURCES = ./src/qsort.c
qsort_OBJECTS = $(qsort_SOURCES:.c=.o)

all: $(BINARIES)

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

qsort: $(qsort_OBJECTS)
	$(CC) $(CFLAGS) -o $@ $+

clean:
	rm -f *.o core.* $(BINARIES) $(qsort_OBJECTS)

#  vim: set ts=4 sw=4 tw=120 noet :

