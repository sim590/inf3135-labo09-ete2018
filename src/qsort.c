#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define BUFLEN 256
#define BLOCK_READ 256
#define MAX_READ 1024*1024

int strcmp0(const void* a, const void* b) {
    return strcmp(*(const char**) a, *(const char**) b);
}

int main(int argc, char *argv[]) {
    unsigned int lalloc = 0, lcount = 0, read = 0;
    char line_buf[BUFLEN];

    lalloc += BLOCK_READ;
    char** capitals = calloc(BLOCK_READ, sizeof(char*));

    while (fgets(line_buf, BUFLEN, stdin) && read < MAX_READ) {
        const unsigned int r = strlen(line_buf)+1;
        read += r;
        capitals[lcount] = calloc(r, sizeof(char));

        const char* capital = strtok(line_buf, "\n");
        strcpy(capitals[lcount++], capital);
#ifdef DEBUG
        printf("%s\n", line_buf);
#endif
        if (lcount == lalloc) {
            lalloc *= 2;
            capitals = realloc(capitals, lalloc);
        }
    }

    qsort(capitals, lcount, sizeof(char*), strcmp0);

#ifdef DEBUG
    printf("==================================================\n");
    for (size_t i = 0; i < lcount; ++i) {
        printf("%s\n", capitals[i]);
    }
#endif

    return 0;
}

/* vim: set ts=4 sw=4 tw=120 et :*/

